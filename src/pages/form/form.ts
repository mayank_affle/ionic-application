import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { Item } from '../../models/item';
import { Items } from '../../providers/providers';

import * as firebase from "firebase";

@IonicPage()
@Component({
  selector: 'page-form',
  templateUrl: 'form.html'
})
export class FormPage {

  data : any; 

  constructor(public navCtrl: NavController, public items: Items) {    
  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
  }

  ngOnInit() {

    return firebase.database().ref().once('value').then( (snapshot) => {
      var heroes = snapshot.val().subCategory;
      console.log('-----------' , heroes);
      var data = heroes[Object.keys(heroes)[0]];
      // console.log(data);
      this.data = data.one.questionText
      console.log(this.data)      
    });
  }

  submitAns(){
    alert('Record saved successfully');
  }

  fileUpload(event) {
    let fd = new FormData();
    fd.append('file', event.srcElement.files[0]);
    alert(fd);
  }

  heights = [1,2,3,4,5,6,7,8,9,10];
  volumes = [100,200,300,400,500,600,700,800,900,1000]
    
  
  
}
