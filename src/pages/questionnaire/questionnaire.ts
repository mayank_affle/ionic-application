import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { Item } from '../../models/item';
import { Items } from '../../providers/providers';


@IonicPage()
@Component({
  selector: 'question-form',
  templateUrl: 'questionnaire.html'
})
export class QuestionPage {


  constructor(public navCtrl: NavController, public items: Items) {

  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
  }

}
