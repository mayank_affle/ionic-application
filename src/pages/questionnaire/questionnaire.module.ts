import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { QuestionPage } from './questionnaire';

@NgModule({
  declarations: [
    QuestionPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionPage),
    TranslateModule.forChild()
  ],
  exports: [
    QuestionPage
  ]
})
export class QuestionPageModule { }
